import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

class Player {
    private int turn = 0;

    private final Board board = new Board();
    private final Random random = new Random(123);

    private int tier1Count, tier2Count, tier3Count, itemCount;

    static class SummonNode {
        int mana;
        List<Card> cardsToSummon = new LinkedList<>();
        List<Card> deck = new LinkedList<>();

        List<Card> resolve() {
            double value = val(cardsToSummon);
            log("Cards %.2f %s", value, String.join(",", cardsToSummon.stream().map(card -> card.instanceId + "").collect(Collectors.toList())));
            LinkedList<List<Card>> children = new LinkedList<>();
            final List<Card> collect = deck.stream().filter(card -> card.cost <= mana).collect(Collectors.toList());
            for (Card card : collect) {
                final SummonNode summonNode = new SummonNode();
                summonNode.mana = mana - card.cost;
                summonNode.deck.addAll(deck.stream().filter(c -> c.instanceId != card.instanceId).collect(Collectors.toList()));
                summonNode.cardsToSummon.addAll(cardsToSummon);
                summonNode.cardsToSummon.add(card);
                children.add(summonNode.resolve());
            }
            return children.stream().max(Comparator.comparingDouble(SummonNode::val)).orElse(this.cardsToSummon);
        }

        private static Double val(List<Card> list) {
            return list.stream().mapToDouble(Card::value).sum();
        }
    }

    static class Constants {
        final static int DECK_SIZE = 30;
        final static Map<Integer, Double> cardValue = Arrays.stream(new Object[][]{
                {83, 3d}, {84, 5d}, {91, 5d}, {63, 6d}, {92, 4.5d}, {93, 4.5d}, {49, 4d}, {55, 5d}, {94, 5d}, {95, 5d}, {96, 5d}, {40, 6.5d}, {99, 5.5d}, {100, 5.5d}, {38, 5.5d}, {47, 6d}, {69, 6.5d}, {41, 3.5d}, {86, 2.5d}, {64, 4d}, {97, 4.5d}, {98, 4.5d}, {39, 4.5d}, {85, 1.5d}, {1, 2.5d}, {2, 2.5d}, {3, 2.5d}, {48, 1.5d}, {4, 3d}, {7, 3d}, {26, 3d}, {27, 3d}, {65, 3d}, {30, 3.5d}, {24, 1.5d}, {5, 2d}, {6, 2d}, {8, 2d}, {9, 2.5d}, {11, 2.5d}, {12, 2.5d}, {50, 1.5d}, {25, 1d}, {28, 1d}, {29, 1d}, {10, 2.5d}, {32, 1.5d}, {54, 0.5d}, {31, 0.5d},
                {51, 3d}, {44, 5d}, {52, 1d}, {53, -2d}, {73, 9d}, {87, 5d}, {103, 6d}, {74, 7.5d}, {18, 5d}, {70, 6d}, {104, 5d}, {75, 6.5d}, {105, 5.5d}, {106, 5.5d}, {111, 6d}, {13, 4d}, {14, 4d}, {72, 5d}, {101, 4d}, {102, 4d}, {107, 4.5d}, {110, 4.5d}, {67, 5d}, {68, 5d}, {76, 7d}, {112, 5d}, {15, 3d}, {17, 3d}, {56, 3d}, {57, 3d}, {71, 3d}, {19, 3.5d}, {21, 3.5d}, {108, 3.5d}, {109, 3.5d}, {37, 4d}, {58, 5d}, {113, 4d}, {16, 2d}, {33, 2d}, {42, 3d}, {20, 2.5d}, {88, 1.5d}, {22, 3d}, {43, 4d}, {45, 5d}, {34, 1.5d}, {89, 0.5d}, {66, 0.5d}, {35, 2d}, {36, 1d},
                {80, 11d}, {62, 12d}, {116, 11d}, {114, 6.5d}, {23, 5.5d}, {59, 5.5d}, {77, 6.5d}, {82, 7.5d}, {79, 7d}, {61, 6.5d}, {78, 6d}, {115, 3d}, {60, 1.5d}, {46, 3.5d}, {81, 2.5d}, {90, -1d},
                {151, 102d}, {142, 13d}, {148, 11d}, {149, 8d}, {118, 3d}, {122, 3d}, {143, 3d}, {117, 2d}, {130, 2d}, {136, 2d}, {141, 2d}, {119, 1d}, {153, 1d}, {121, 0d}, {123, 0d}, {127, 0d}, {138, 0d}, {156, 0d}, {160, 0d}, {144, 0d}, {120, -1d}, {124, -1d}, {125, -1d}, {126, -1d}, {128, -1d}, {129, -1d}, {154, -1d}, {150, -1d}, {132, -2d}, {135, -2d}, {137, -2d}, {140, -2d}, {147, -2d}, {145, -2d}, {146, -2d}, {155, -2d}, {159, -2d}, {158, -2d}, {131, -3d}, {134, -3d}, {157, -3d}, {133, -4d}, {139, -4d}, {152, -6d},
        }).collect(Collectors.toMap(kv -> (Integer) kv[0], kv -> (Double) kv[1]));
        final static List<Integer> tier1 = Arrays.stream(new Object[][]{
                {83, 3d}, {84, 5d}, {91, 5d}, {63, 6d}, {92, 4.5d}, {93, 4.5d}, {49, 4d}, {55, 5d}, {94, 5d}, {95, 5d}, {96, 5d}, {40, 6.5d}, {99, 5.5d}, {100, 5.5d}, {38, 5.5d}, {47, 6d}, {69, 6.5d}, {41, 3.5d}, {86, 2.5d}, {64, 4d}, {97, 4.5d}, {98, 4.5d}, {39, 4.5d}, {85, 1.5d}, {1, 2.5d}, {2, 2.5d}, {3, 2.5d}, {48, 1.5d}, {4, 3d}, {7, 3d}, {26, 3d}, {27, 3d}, {65, 3d}, {30, 3.5d}, {24, 1.5d}, {5, 2d}, {6, 2d}, {8, 2d}, {9, 2.5d}, {11, 2.5d}, {12, 2.5d}, {50, 1.5d}, {25, 1d}, {28, 1d}, {29, 1d}, {10, 2.5d}, {32, 1.5d}, {54, 0.5d}, {31, 0.5d},
        }).map(kv -> (Integer) kv[0]).collect(Collectors.toList());
        final static List<Integer> tier2 = Arrays.stream(new Object[][]{
                {51, 3d}, {44, 5d}, {52, 1d}, {53, -2d}, {73, 9d}, {87, 5d}, {103, 6d}, {74, 7.5d}, {18, 5d}, {70, 6d}, {104, 5d}, {75, 6.5d}, {105, 5.5d}, {106, 5.5d}, {111, 6d}, {13, 4d}, {14, 4d}, {72, 5d}, {101, 4d}, {102, 4d}, {107, 4.5d}, {110, 4.5d}, {67, 5d}, {68, 5d}, {76, 7d}, {112, 5d}, {15, 3d}, {17, 3d}, {56, 3d}, {57, 3d}, {71, 3d}, {19, 3.5d}, {21, 3.5d}, {108, 3.5d}, {109, 3.5d}, {37, 4d}, {58, 5d}, {113, 4d}, {16, 2d}, {33, 2d}, {42, 3d}, {20, 2.5d}, {88, 1.5d}, {22, 3d}, {43, 4d}, {45, 5d}, {34, 1.5d}, {89, 0.5d}, {66, 0.5d}, {35, 2d}, {36, 1d},
        }).map(kv -> (Integer) kv[0]).collect(Collectors.toList());
        final static List<Integer> tier3 = Arrays.stream(new Object[][]{
                {80, 11d}, {62, 12d}, {116, 11d}, {114, 6.5d}, {23, 5.5d}, {59, 5.5d}, {77, 6.5d}, {82, 7.5d}, {79, 7d}, {61, 6.5d}, {78, 6d}, {115, 3d}, {60, 1.5d}, {46, 3.5d}, {81, 2.5d}, {90, -1d},
        }).map(kv -> (Integer) kv[0]).collect(Collectors.toList());
        final static List<Integer> items = Arrays.stream(new Object[][]{
                {151, 102d}, {142, 13d}, {148, 11d}, {149, 8d}, {118, 3d}, {122, 3d}, {143, 3d}, {117, 2d}, {130, 2d}, {136, 2d}, {141, 2d}, {119, 1d}, {153, 1d}, {121, 0d}, {123, 0d}, {127, 0d}, {138, 0d}, {156, 0d}, {160, 0d}, {144, 0d}, {120, -1d}, {124, -1d}, {125, -1d}, {126, -1d}, {128, -1d}, {129, -1d}, {154, -1d}, {150, -1d}, {132, -2d}, {135, -2d}, {137, -2d}, {140, -2d}, {147, -2d}, {145, -2d}, {146, -2d}, {155, -2d}, {159, -2d}, {158, -2d}, {131, -3d}, {134, -3d}, {157, -3d}, {133, -4d}, {139, -4d}, {152, -6d},
        }).map(kv -> (Integer) kv[0]).collect(Collectors.toList());

        final static int TIER_1_TARGET = 10;
        final static int TIER_2_TARGET = 14;
        final static int TIER_3_TARGET = 6;
        final static int ITEM_TARGET = 0;

    }

    public static void main(String args[]) {
        assert Constants.cardValue.size() == 160;
        for (int i = 0; i < 160; i++) {
            assert Constants.cardValue.containsKey(i + 1);
        }
        Scanner in = new Scanner(System.in);
        Player player = new Player();
        player.play(in);


    }

    private void play(Scanner in) {

        // game loop
        while (true) {
            log("turn %d", turn);
            board.me.update(in);
//            log("Me: %s", me);
            board.enemy.update(in);
//            log("Enemy: %s", enemy);

            board.me.hand.clear();
            board.boardMe.clear();
            board.boardEnemy.clear();

            board.enemy.cards = in.nextInt();
            final int cardCount = in.nextInt();
            for (int i = 0; i < cardCount; i++) {
                final Card card = Card.from(in);
//                log("%s", card);
                switch (card.location) {
                    case 0:
                        board.me.hand.add(card);
                        break;
                    case 1:
                        board.boardMe.add(card);
                        break;
                    case -1:
                        board.boardEnemy.add(card);
                        break;
                    default:
                        throw new IllegalStateException("Invalid card location " + card.location);
                }
            }


            if (turn < Constants.DECK_SIZE) {
                final Action action = getDraftAction();
                System.out.println(action);
            } else {
                List<Action> actions = getBattleActions();
                final String collect = actions.stream().map(Object::toString).collect(Collectors.joining(";"));
                System.out.println(collect);
            }
            turn++;
        }

    }

    private Action getDraftAction() {
        double t1 = 0, t2 = 0, t3 = 0, i = 0;

        if (board.me.hand.stream().anyMatch(Card::isTier1)) {
            t1 = Math.max(0, Constants.TIER_1_TARGET - tier1Count);
        }
        if (board.me.hand.stream().anyMatch(Card::isTier2)) {
            t2 = Math.max(0, Constants.TIER_2_TARGET - tier2Count);
        }
        if (board.me.hand.stream().anyMatch(Card::isTier3)) {
            t3 = Math.max(0, Constants.TIER_3_TARGET - tier3Count);
        }
        if (board.me.hand.stream().anyMatch(Card::isItem)) {
            i = Math.max(0, Constants.ITEM_TARGET - itemCount);
        }
        final Optional<Card> maxTier1 = board.me.hand.stream().filter(Card::isTier1).max(Comparator.comparingInt(Card::tierRank));
        if (maxTier1.isPresent()) {
            int size = Constants.tier1.size();
            t1 *= size - maxTier1.get().tierRank();
            t1 /= size;
        }
        final Optional<Card> maxTier2 = board.me.hand.stream().filter(Card::isTier2).max(Comparator.comparingInt(Card::tierRank));
        if (maxTier2.isPresent()) {
            int size = Constants.tier2.size();
            t2 *= size - maxTier2.get().tierRank();
            t2 /= size;
        }
        final Optional<Card> maxTier3 = board.me.hand.stream().filter(Card::isTier3).max(Comparator.comparingInt(Card::tierRank));
        if (maxTier3.isPresent()) {
            int size = Constants.tier3.size();
            t3 *= size - maxTier3.get().tierRank();
            t3 /= size;
        }
        final Optional<Card> maxItem = board.me.hand.stream().filter(Card::isItem).max(Comparator.comparingInt(Card::tierRank));
        if (maxItem.isPresent()) {
            int size = Constants.items.size();
            i *= size - maxItem.get().tierRank();
            i /= size;
        }

        log("1: %d/%d, 2: %d/%d, 3:%d/%d, i:%d/%d",
                tier1Count, Constants.TIER_1_TARGET,
                tier2Count, Constants.TIER_2_TARGET,
                tier3Count, Constants.TIER_3_TARGET,
                itemCount, Constants.ITEM_TARGET
        );
        log("1: %.1f, 2: %.1f, 3:%.1f, i:%.1f", t1, t2, t3, i);

        // todo: factor in relative strength in tier, bayesian?

        double bound = t1 + t2 + t3 + i;
        if (bound <= 0) {
            final Card first = board.me.hand.stream().max(Comparator.comparingDouble(Card::value)).orElseThrow(IllegalStateException::new);
            log("Picked %s", first);
            return new PickAction(board.me.hand.indexOf(first));
        }

        double pick = random.nextDouble() * bound;
        if (pick < t1 && t1 > 0) {
            log("Pick t1 card");
            tier1Count++;
            final Card first = board.me.hand.stream().filter(Card::isTier1).max(Comparator.comparingDouble(Card::value)).orElseThrow(IllegalStateException::new);
            log("Picked %s", first);
            return new PickAction(board.me.hand.indexOf(first));
        } else if (pick < t1 + t2 && t2 > 0) {
            log("Pick t2 card");
            tier2Count++;
            final Card first = board.me.hand.stream().filter(Card::isTier2).max(Comparator.comparingDouble(Card::value)).orElseThrow(IllegalStateException::new);
            log("Picked %s", first);
            return new PickAction(board.me.hand.indexOf(first));
        } else if (pick < t1 + t2 + t3 && t3 > 0) {
            log("Pick t3 card");
            tier3Count++;
            final Card first = board.me.hand.stream().filter(Card::isTier3).max(Comparator.comparingDouble(Card::value)).orElseThrow(IllegalStateException::new);
            log("Picked %s", first);
            return new PickAction(board.me.hand.indexOf(first));
        } else {
            log("Pick item card");
            itemCount++;
            final Card first = board.me.hand.stream().filter(Card::isItem).max(Comparator.comparingDouble(Card::value)).orElseThrow(IllegalStateException::new);
            log("Picked %s", first);
            return new PickAction(board.me.hand.indexOf(first));
        }

    }

    private List<Action> getBattleActions() {
        final List<Action> manaActions = getManaActions();

        final List<Action> actions = new LinkedList<>();
        final List<Action> chargers = manaActions.stream()
                .filter(action -> action instanceof SummonAction)
                .map(action -> (SummonAction) action)
                .filter(action -> action.card.charge)
                .collect(Collectors.toList());

        List<Action> executed = new LinkedList<>();

        // chargers we can play now
        for (Action action : chargers) {
            if (board.action(action)) {
                executed.add(action);
                actions.add(action);
            }
        }
        chargers.removeAll(executed);
        manaActions.removeAll(executed);
        executed.clear();

        // use actions
        for (Action action : manaActions) {
            if (board.action(action)) {
                executed.add(action);
                actions.add(action);
            }
        }
        chargers.removeAll(executed);
        manaActions.removeAll(executed);
        executed.clear();

        final List<Card> availableToAttack = board.availableToAttack();

        List<Card> guards = board.enemyGuards();
        if (guards.size() > 0) {
            log("Guards %d", guards.size());
            List<AttackAction> guardActions = getKillGuardsActions(availableToAttack, guards);
            for (AttackAction guardAction : guardActions) {
                actions.add(guardAction);
                Card attacker = availableToAttack.stream().filter(card -> card.instanceId == guardAction.attacker).findFirst().orElseThrow(IllegalStateException::new);
                Card guard = guards.stream().filter(card -> card.instanceId == guardAction.defender).findFirst().orElseThrow(IllegalStateException::new);
                board.attack(attacker, guard);
                availableToAttack.remove(attacker);
            }
        }


        while (availableToAttack.size() > 0) {
            if (availableToAttack.stream().map(card -> card.attack).mapToInt(x -> x).sum() >= board.enemy.health) {
                log("Finishing blow");
                final List<AttackAction> finish = availableToAttack.stream().map(card -> new AttackAction(card.instanceId, -1, "Finishing blow")).collect(Collectors.toList());
                actions.addAll(finish);
                break;
            }

            final List<Card> byThreat = board.boardEnemy.stream()
                    .filter(card -> !card.isDead())
                    .sorted(Card::threat).collect(Collectors.toList());
            if (byThreat.size() == 0) {
                // no threats, just attack enemy with all we got
                final List<AttackAction> finish = availableToAttack.stream().map(card -> new AttackAction(card.instanceId, -1, "")).collect(Collectors.toList());
                actions.addAll(finish);
                break;
            } else {
                // cards still on table, attack cards or enemy
                if (availableToAttack.stream().allMatch(Card::hasGuard)) {
                    // just guards remaining, attack with them if necessary
                    break;
                    /*
                    Card attacker = availableToAttack.get(0);
                    Card defender = getTargetFor(attacker, byThreat.stream().filter(Card::hasGuard).collect(Collectors.toList()));
                    actions.add(new AttackAction(attacker.instanceId, defender == null ? -1 : defender.instanceId));
                    board.attack(attacker, defender);
                    availableToAttack.remove(attacker);
                    continue;*/
                }

                final Optional<Card> breakThrough = availableToAttack.stream().filter(Card::hasBreakthrough).findFirst();
                if (breakThrough.isPresent()) {
                    Card attacker = breakThrough.get();
                    log("breakthrough %d", attacker.instanceId);
                    final Card defender = byThreat.stream().min(Comparator.comparingInt(card -> card.defense)).orElseThrow(IllegalStateException::new);
                    actions.add(new AttackAction(attacker.instanceId, defender.instanceId));
                    board.attack(attacker, defender);
                    availableToAttack.remove(attacker);
                    continue;
                }

                final Optional<Card> lethal = availableToAttack.stream().filter(Card::hasLethal).findFirst();
                if (lethal.isPresent()) {
                    Card attacker = lethal.get();
                    log("lethal %d", attacker.instanceId);
                    final Optional<Card> defender = byThreat.stream()
                            .filter(card -> !card.ward)
                            .max(Comparator.comparingInt(card -> card.defense));
                    if (defender.isPresent()) {
                        actions.add(new AttackAction(attacker.instanceId, defender.get().instanceId));
                        board.attack(attacker, defender.get());
                        availableToAttack.remove(attacker);
                        continue;
                    }
                }

                Card attacker = availableToAttack.stream().filter(card -> !card.guard).findFirst().orElseThrow(IllegalStateException::new);
                Card defender = getTargetFor(attacker, byThreat);
                actions.add(new AttackAction(attacker.instanceId, defender == null ? -1 : defender.instanceId));
                board.attack(attacker, defender);
                availableToAttack.remove(attacker);
            }
        }

        // rest of the cards we have room for
        for (Action action : manaActions) {
            if (board.action(action)) {
                executed.add(action);
                actions.add(action);
            }
        }
        chargers.removeAll(executed);
        manaActions.removeAll(executed);
        executed.clear();


        if (actions.size() == 0) {
            actions.add(new PassAction());
        }
        return actions;
    }

    private Card getTargetFor(Card attacker, List<Card> byThreat) {
        if (attacker.isStrongKiller()) {
            return null; // attack enemy directly
        }
        final Optional<Card> first = byThreat.stream().filter(defender -> attacker.defense > defender.attack).findFirst();
        // attack biggest threat that leaves me alive, otherwise attack enemy
        return first.orElse(null);
    }

    private List<AttackAction> getKillGuardsActions(List<Card> availableToAttack, List<Card> guards) {
        List<AttackAction> actions = new LinkedList<>();

        List<Card> sortedAttackers = availableToAttack.stream()
                .filter(card -> !card.guard) // don't attack with guards...
                .sorted(Comparator.comparingInt(card -> card.attack)).collect(Collectors.toList());

        List<Card> sortedGuards = guards.stream().sorted(Comparator.comparingInt(card -> -card.defense)).collect(Collectors.toList());
        while (sortedGuards.size() > 0 && sortedAttackers.size() > 0) {
            Card defender = sortedGuards.get(0);
            final Card attacker = sortedAttackers.stream()
                    .filter(card -> card.attack >= defender.defense)
                    .findFirst() // first that can kill
                    .orElse(sortedAttackers.get(sortedAttackers.size() - 1)); // or the strongest one
            actions.add(new AttackAction(attacker.instanceId, defender.instanceId));
            sortedAttackers.remove(attacker);
            if (defender.defense < attacker.attack) {
                sortedGuards.remove(defender);
            }
        }
        return actions;
    }

    private List<Action> getManaActions() {
        final SummonNode summonNode = new SummonNode();
        summonNode.mana = board.me.mana;
        summonNode.deck.addAll(board.me.hand);
        List<Card> foo = summonNode.resolve();
        for (Card card : foo) {
            log("Should summon %s", card);
        }

        int mana = board.me.mana;
        final List<Action> actions = new LinkedList<>();

        int finalMana = mana;
        List<Card> summonableCards = board.me.hand.stream()
                .filter(card -> card.cost <= finalMana)
                .sorted(Comparator.comparingDouble(Card::value).reversed())
                .collect(Collectors.toList());

        while (summonableCards.size() > 0) {
            final Card card = summonableCards.get(0);

            log("Highest value %s", card);
            if (card.isBlue()) {
                actions.add(new UseAction(card, null));
                mana -= card.cost;
            } else if (card.isGreen()) {
                List<Card> targets = board.boardMe.stream().sorted(Comparator.comparingDouble(Card::value)).collect(Collectors.toList());
                Optional<Card> target = Optional.empty();
                if (card.guard) {
                    target = targets.stream().filter(c -> !c.guard).findFirst();
                } else if (card.ward) {
                    target = targets.stream().filter(c -> !c.ward).findFirst();
                } else if (card.drain) {
                    target = targets.stream().filter(c -> !c.drain).findFirst();
                } else if (card.breakthrough) {
                    target = targets.stream().filter(c -> !c.breakthrough).findFirst();
                } else if (card.lethal) {
                    target = targets.stream().filter(c -> !c.lethal).findFirst();
                }

                if (target.isPresent()) {
                    actions.add(new UseAction(card, target.get()));
                    mana -= card.cost;
                }
            } else if (card.isRed()) {
                List<Card> targets = board.boardEnemy.stream().sorted(Comparator.comparingDouble(Card::value)).collect(Collectors.toList());
                Optional<Card> target = Optional.empty();
                if (card.guard) {
                    target = targets.stream().filter(Card::hasGuard).findFirst();
                } else if (card.ward) {
                    target = targets.stream().filter(Card::hasWard).findFirst();
                } else if (card.drain) {
                    target = targets.stream().filter(Card::hasDrain).findFirst();
                } else if (card.breakthrough) {
                    target = targets.stream().filter(Card::hasBreakthrough).findFirst();
                } else if (card.lethal) {
                    target = targets.stream().filter(Card::hasLethal).findFirst();
                }

                if (target.isPresent()) {
                    log("red target %s", target.get());
                    actions.add(new UseAction(card, target.get()));
                    mana -= card.cost;
                } else {
                    log("no target for red");
                }
            } else if (card.isCreature()) {
                actions.add(new SummonAction(card));
                mana -= card.cost;
            }
            summonableCards.remove(card);

            int finalMana1 = mana;
            summonableCards = summonableCards.stream()
                    .filter(c -> c.cost <= finalMana1)
                    .sorted(Comparator.comparingDouble(Card::value).reversed())
                    .collect(Collectors.toList());
        }

        return actions;
    }


    private static void log(String format, Object... args) {
        System.err.println(String.format(format, args));
    }

    interface Action {
    }

    static class Card {
        int cardNumber;
        int instanceId;
        int location;
        int cardType;
        int cost;
        int attack;
        int defense;
        int myHealthChange;
        int opponentHealthChange;
        int cardDraw;
        boolean canAttack = true;
        boolean charge, breakthrough, ward, drain, lethal, guard;

        static Card from(Scanner in) {
            final Card card = new Card();

            card.cardNumber = in.nextInt();
            card.instanceId = in.nextInt();
            card.location = in.nextInt();
            card.cardType = in.nextInt();
            card.cost = in.nextInt();
            card.attack = in.nextInt();
            card.defense = in.nextInt();
            String abilities = in.next();
            card.charge = abilities.contains("C");
            card.breakthrough = abilities.contains("B");
            card.ward = abilities.contains("W");
            card.drain = abilities.contains("D");
            card.lethal = abilities.contains("L");
            card.guard = abilities.contains("G");

            card.myHealthChange = in.nextInt();
            card.opponentHealthChange = in.nextInt();
            card.cardDraw = in.nextInt();
            return card;
        }

        @Override
        public String toString() {
            return "Card{" +
                    "cardNumber=" + cardNumber +
                    ", instanceId=" + instanceId +
                    ", location=" + location +
                    ", cardType=" + cardType +
                    ", cost=" + cost +
                    ", attack=" + attack +
                    ", defense=" + defense +
                    ", myHealthChange=" + myHealthChange +
                    ", opponentHealthChange=" + opponentHealthChange +
                    ", cardDraw=" + cardDraw +
                    '}';
        }

        boolean isCreature() {
            return cardType == 0;
        }

        boolean isGreen() {
            return cardType == 1;
        }

        boolean isRed() {
            return cardType == 2;
        }

        boolean isBlue() {
            return cardType == 3;
        }

        static int threat(Card card1, Card card2) {
            return card2.attack - card1.attack;
        }

        static boolean hasCharge(Card card) {
            return card.charge;
        }

        static boolean hasBreakthrough(Card card) {
            return card.breakthrough;
        }

        static boolean hasDrain(Card card) {
            return card.drain;
        }

        static boolean hasLethal(Card card) {
            return card.lethal;
        }

        static boolean hasWard(Card card) {
            return card.ward;
        }

        static boolean hasGuard(Card card) {
            return card.guard;
        }

        boolean canKill(Card threat) {
            return this.attack >= threat.defense;
        }

        static double value(Card card) {
            return Constants.cardValue.get(card.cardNumber);
        }

        static int overKill(Card card1, Card card2) {
            return card1.defense - card2.attack + card2.defense - card1.attack;
        }

        boolean isDead() {
            return defense <= 0;
        }

        boolean isTier1() {
            return isCreature() && cost <= 3;
        }

        boolean isTier2() {
            return isCreature() && cost > 3 && cost <= 6;
        }

        boolean isTier3() {
            return isCreature() && cost > 6;
        }

        boolean isItem() {
            return !isCreature();
        }

        boolean isStrongKiller() {
            return attack - 3 > defense;
        }

        boolean isStrongDefender() {
            return defense - 3 > attack;
        }

        int tierRank() {
            if (isTier1()) {
                return Constants.tier1.indexOf(cardNumber);
            }
            if (isTier2()) {
                return Constants.tier2.indexOf(cardNumber);
            }
            if (isTier3()) {
                return Constants.tier3.indexOf(cardNumber);
            }
            return Constants.items.indexOf(cardNumber);
        }
    }

    static class Participant {
        int health;
        int mana;
        int deck;
        int rune;
        int cards;
        final List<Card> hand = new LinkedList<>();

        void update(Scanner in) {
            health = in.nextInt();
            mana = in.nextInt();
            deck = in.nextInt();
            rune = in.nextInt();
        }

        @Override
        public String toString() {
            return "Participant{" +
                    "health=" + health +
                    ", mana=" + mana +
                    ", deck=" + deck +
                    ", rune=" + rune +
                    '}';
        }
    }

    static class AttackAction implements Action {
        final int attacker, defender;
        String log = "";

        AttackAction(int attacker, int defender) {
            this.attacker = attacker;
            this.defender = defender;
        }

        AttackAction(int attacker, int defender, String log) {
            this(attacker, defender);
            this.log = log;
        }

        @Override
        public String toString() {
            if (log != null) {
                return String.format("ATTACK %d %d %s", attacker, defender, log);
            }
            return String.format("ATTACK %d %d", attacker, defender);
        }
    }

    static class SummonAction implements Action {
        final Card card;

        SummonAction(Card card) {
            this.card = card;
        }

        @Override
        public String toString() {
            return String.format("SUMMON %d", card.instanceId);
        }
    }

    static class UseAction implements Action {
        final Card card;
        final Card target;

        UseAction(Card card, Card target) {
            this.card = card;
            this.target = target;
        }

        @Override
        public String toString() {
            return String.format("USE %d %d", card.instanceId, target == null ? -1 : target.instanceId);
        }
    }

    static class PickAction implements Action {
        final int index;

        PickAction(int index) {
            this.index = index;
        }

        @Override
        public String toString() {
            return "PICK " + index;
        }
    }

    static class PassAction implements Action {
        @Override
        public String toString() {
            return "PASS pass";
        }
    }

    static class Board {
        final Participant me = new Participant();
        final Participant enemy = new Participant();

        final List<Card> boardMe = new LinkedList<>();
        final List<Card> boardEnemy = new LinkedList<>();

        List<Card> enemyGuards() {
            return boardEnemy.stream()
                    .filter(Card::hasGuard)
                    .filter(card -> !card.isDead())
                    .sorted(Card::threat)
                    .collect(Collectors.toList());
        }

        void attack(Card attacker, Card defender) {
            if (defender == null) {
                attack(attacker);
                return;
            }

            if (!defender.ward) {
                defender.defense -= attacker.attack;

                if (attacker.breakthrough && defender.defense < attacker.attack) {
                    enemy.health -= (attacker.attack - defender.defense);
                }

                if (attacker.lethal && attacker.attack > 0) {
                    defender.defense -= 99;
                }

                if (attacker.drain) {
                    me.health += Math.min(attacker.attack, defender.defense);
                }
            }

            if (!attacker.ward) {
                attacker.defense -= defender.attack;
            }

            if (attacker.isDead()) {
                log("Dead: %s", attacker);
                boardMe.remove(attacker);
                boardEnemy.remove(attacker);
            }
            if (defender.isDead()) {
                log("Dead: %s", defender);
                boardMe.remove(defender);
                boardEnemy.remove(defender);
            }

            attacker.ward = false;
            defender.ward = false;
        }

        void attack(Card attacker) {
            enemy.health -= attacker.attack;
        }

        List<Card> availableToAttack() {
            return boardMe.stream().filter(card -> card.attack > 0).filter(card -> card.canAttack).collect(Collectors.toList());
        }

        void use(Card card, Card target) {
            enemy.health += card.opponentHealthChange;
            me.health += card.myHealthChange;

            if (card.isRed()) {
                assert target != null;
                // remove features
                if (card.guard) target.guard = false;
                if (card.ward) target.ward = false;
                if (card.charge) target.charge = false;
                if (card.breakthrough) target.breakthrough = false;
                if (card.drain) target.drain = false;
                if (card.lethal) target.lethal = false;

                target.defense += card.defense;
                target.attack += card.attack;
            }

            if (card.isGreen()) {
                assert target != null;
                // add features
                if (card.guard) target.guard = true;
                if (card.ward) target.ward = true;
                if (card.charge) target.charge = true;
                if (card.breakthrough) target.breakthrough = true;
                if (card.drain) target.drain = true;
                if (card.lethal) target.lethal = true;

                target.defense += card.defense;
                target.attack += card.attack;
            }

            if (card.isBlue()) {
                if (target == null) {
                    enemy.health += card.defense;
                } else {
                    target.defense += card.defense;
                }
            }

        }

        void summon(Card card) {
            card.canAttack = false;
            me.hand.add(card);
        }

        boolean action(Action action) {
            if (action instanceof UseAction) {
                UseAction useAction = (UseAction) action;
                use(useAction.card, useAction.target);
                return true;
            } else if (action instanceof SummonAction) {
                if (boardMe.size() < 6) {
                    SummonAction summonAction = (SummonAction) action;
                    summon(summonAction.card);
                    return true;
                }
                log("Board full, can't play");
                return false;
            } else {
                throw new IllegalStateException();
            }

        }
    }

}
